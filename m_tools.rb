# frozen_string_literal: true

require_relative 'app/application'

# M-Tool Application
begin
  # Get Module Name
  module_name = if ARGV[0].nil? || ARGV[0].empty?
                  'empty'
                else
                  ARGV[0]
                end
  # create new Application class
  app = Application.new
  # Run Application
  app.run module_name
  # Finish Application
  app.finish
end
