# frozen_string_literal: true

# MySQL DB Engine Implementation
class MySQL < DatabaseInterface
  # Class constructor
  # @param [DatabaseConfiguration] configuration
  # @raise
  def initialize(configuration)
    @configuration = configuration
    connect configuration
  end

  # Run Select Script
  # @param [string] query
  # @param [string] argv
  # @return Hash
  def db_select(query, *argv)
    connect @configuration if @client.closed?
    # prepare sql query
    sql = format(query, *argv)
    # exec SQL query
    result = @client.query(sql, as: :hash)
    # return result
    result.each
  rescue StandardError => e
    log(LOG_ERROR, e.message)
    log_save(LOG_DEBUG, query)
  ensure
    close
  end

  # Run Select Script and return first line
  # @param [string] query
  # @param [string] argv
  # @return Hash
  def db_select_first(query, *argv)
    connect @configuration if @client.closed?
    # get mysql result
    result = db_select(query, *argv)
    result if result.empty? == true
    # get first line of result
    result[0]
  end

  # Run Select Script and return parameter from first line
  # @param [string] query
  # @param [string] parameter
  # @param [string] argv
  # @return string
  def db_select_first_parameter(query, parameter, *argv)
    connect @configuration if @client.closed?
    # get first line
    result = db_select_first(query, *argv)
    # get parameter or return nil
    result[parameter] if result.include?(parameter) == true
  end

  # Run Insert Script
  # @param [string] query
  # @param [string] argv
  # @return boolean
  def db_insert(query, *argv)
    connect @configuration if @client.closed?
    # prepare sql query
    sql = format(query, *argv)
    # exec SQL query
    @client.query(sql, as: :hash)
    # return result
    true
  rescue StandardError => e
    log(LOG_ERROR, e.message)
    log_save(LOG_DEBUG, query)
    false
  ensure
    close
  end

  # Run Update Script
  # @param [string] query
  # @param [string] argv
  # @return boolean
  def db_update(query, *argv)
    db_insert(query, argv)
  end

  # Run Delete Script
  # @param [string] query
  # @param [string] argv
  # @return boolean
  def db_delete(query, *argv)
    db_insert(query, argv)
  end

  private

  # Open new MySQL Connection
  # @raise
  def connect(configuration)
    @client = Mysql2::Client.new(host: configuration.host,
                                 username: configuration.username,
                                 password: configuration.password,
                                 database: configuration.database)
  end

  # Close MySQL Connection
  def close
    @client.close if @client.closed? == false
  end
end