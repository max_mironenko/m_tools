# frozen_string_literal: true

# Class contains Database Configuration Parameters
# Uses for Initialize database connection
class DatabaseConfiguration
  attr_reader :host
  attr_reader :username
  attr_reader :password
  attr_reader :database
  attr_reader :engine

  # Class Constructor
  # @param [string] host
  # @param [string] username
  # @param [string] password
  # @param [string] database
  # @param [string] engine
  def initialize(host, username, password, database, engine)
    @host = host
    @username = username
    @password = password
    @database = database
    @engine = engine
  end
end