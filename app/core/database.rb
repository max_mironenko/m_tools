# frozen_string_literal: true

require_relative 'db/mysql'

# Create DB Engine Class Instance
class Database
  attr_reader :client

  # Class constructor
  # @param [DatabaseConfiguration] configuration
  # @return DatabaseInterface
  # @raise
  def initialize(configuration)
    # only one engine supported by m-tools
    raise 'Database engine is not supported' if configuration.engine != 'mysql'

    @client = MySQL.new(configuration)
  end
end