# frozen_string_literal: true

require 'json'

require_relative 'database_configuration'
require_relative 'database_interface'
require_relative 'database'

# Class loads application configuration from JSON file
class Configuration
  # class variables
  attr_reader :name, :description, :enable, :additional
  attr_reader :module

  # Class Constructor
  # @param [string] file_path Configuration File Path
  def initialize(file_path)
    @additional = {}
    # Load Application Basic Configuration
    load_configuration file_path

    # check if module is enable
    raise 'This module is not enable' if @enable != true

    # load class
    load_class

    # set class parameters
    @module.name = @name
    @module.description = @description
    @module.parameters = @additional

    # Load Database configuration if possible
    if @parameters.include?('database') == true
      @module.database = Database.new(load_database_configuration(@parameters['database']))
    end
  end

  private

  # Load Application Basic Configuration
  # @param [string] file_path Path to configuration file
  # @raise
  def load_configuration(file_path)
    # Load Application Configuration
    @parameters = load_json_data file_path

    # Check basic parameters
    if @parameters.include?('name') == false
      raise 'Required parameter "name" not found in the module configuration file'
    end

    # list of basic parameters
    parameter_list = %w[name description enable]
    # set basic class parameters
    parameter_list.each do |e|
      instance_variable_set("@#{e}", @parameters.include?(e) == true ? @parameters[e] : '')
    end

    # add service parameters
    parameter_list.push('class_file', 'class_name', 'database')
    # set extra class parameters
    @parameters.each do |key, value|
      @additional.store(key, value) if parameter_list.include?(key) == false
    end
  end

  # Load Main Class for Module
  def load_class
    # load class file
    if @parameters.include?('class_file') == false
      raise 'Required parameter "class_file" not found in the module configuration file'
    end

    # Get Class file path
    class_path = Dir.pwd + '/app/modules' + @parameters['class_file']
    raise 'File with Module not found on the file system' if File.exist?(class_path + '.rb') == false

    if @parameters.include?('class_name') == false
      raise 'Required parameter "class_name" not found in the module configuration file'
    end

    # Include class (file) source code
    require_relative class_path
    # Load class
    @module = (Object.const_get @parameters['class_name']).new
    raise 'The Module is not ready for use in the Application' if defined?(@module.check).nil?
    # check class
    raise 'The Module is not ready for use in the Application' if @module.check != true
  end

  # Load Database Configuration
  # @param [string] file_path Configuration File Path
  # @raise
  def load_database_configuration(file_path)
    # Load JSON Configuration
    db_data = load_json_data file_path

    # check if all parameters is set
    %w[host username password database engine].each do |e|
      raise "Parameter #{e} not found in the Database configuration file" if db_data.include?(e) == false
    end

    # Crate new Database Setting Class
    DatabaseConfiguration.new(db_data['host'], db_data['username'],
                              db_data['password'], db_data['database'], db_data['engine'])
  end

  # Load JSON Configuration from File
  # @param [string] file_path Path to configuration file
  # @return [Array] JSON configuration
  # @raise
  def load_json_data(file_path)
    # check if configuration file exist
    raise 'The module configuration file is not exist' if File.exist?(file_path) == false

    # Load configuration data (json) from file
    file = File.open(file_path)
    data = file.read
    file.close

    # Parse Json Data
    JSON.parse data
  end
end
