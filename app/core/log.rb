# frozen_string_literal: true

# Logging Exception Class
class Log
  LOG_ERROR = 'ERROR'
  LOG_WARNING = 'WARNING'
  LOG_INFO = 'INFO'
  LOG_DEBUG = 'DEBUG'

  # Show message on the screen and save into the log file
  # @param[string] type
  # @param[string] message
  # @raise
  def log(type, message)
    log_show(type, message)
    log_save(type, message)
  end

  # Show message on the screen
  # @param[string] type
  # @param[string] message
  # @raise
  def log_show(type, message)
    puts Time.now.inspect + '>>>' + type + ' ::: ' + message
  end

  # Save message into the log file
  # @param[string] type
  # @param[string] message
  # @raise
  def log_save(type, message)
    # get file name
    time = Time.now
    file_name = time.year.to_s + '-' + time.month.to_s + '-' + time.day.to_s + '.log'
    # get file
    file_path = Dir.pwd + '/log/' + file_name

    # save log to file
    file = File.open(file_path, 'a')
    file.puts time.inspect + '>>>' + type + ' ::: ' + message + "\n"
    file.close
  end
end