# frozen_string_literal: true

require 'mysql2'

require_relative 'helper'

# Database interface
class DatabaseInterface < Helper
  # Class constructor
  # @param [DatabaseConfiguration] configuration
  # @raise
  def initialize(configuration)
    raise 'The method is not implemented'
  end

  # Run Select Script
  # @param [string] query
  # @param [string] argv
  # @return Hash
  def db_select(query, *argv)
    raise 'The method is not implemented'
  end

  # Run Select Script and return first line
  # @param [string] query
  # @param [string] argv
  # @return Hash
  def db_select_first(query, *argv)
    raise 'The method is not implemented'
  end

  # Run Select Script and return parameter from first line
  # @param [string] query
  # @param [string] parameter
  # @param [string] argv
  # @return String
  def db_select_first_parameter(query, parameter, *argv)
    raise 'The method is not implemented'
  end

  # Run Insert Script
  # @param [string] query
  # @param [string] argv
  # @return boolean
  def db_insert(query, *argv)
    raise 'The method is not implemented'
  end

  # Run Update Script
  # @param [string] query
  # @param [string] argv
  # @return boolean
  def db_update(query, *argv)
    raise 'The method is not implemented'
  end

  # Run Delete Script
  # @param [string] query
  # @param [string] argv
  # @return boolean
  def db_delete(query, *argv)
    raise 'The method is not implemented'
  end
end