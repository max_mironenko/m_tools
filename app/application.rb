# frozen_string_literal: true

require_relative 'core/configuration'
require_relative 'core/helper'

# Class Create
class Application < Helper
  # Run application
  # @param [string] module_name_str
  def run(module_name_str)
    log(LOG_INFO, 'Application START')
    load_module module_name_str
  rescue StandardError => e
    log(LOG_ERROR, e.message)
  end

  # Finish Application
  def finish
    log(LOG_INFO, 'Application Finished')
  rescue StandardError => e
    log(LOG_ERROR, e.message)
  end

  private

  # Load Module from Application Settings
  def load_module(module_name_str)
    # find extra command for module
    command_index = module_name_str.index(':')
    # extract module name and extra commands
    if command_index.nil?
      command = ''
      command_info = ''
      module_name = module_name_str
    else
      command = module_name_str[command_index + 1..-1]
      command_info = ' with command ' + command
      module_name = module_name_str[0..command_index - 1]
    end

    # Load module
    log(LOG_INFO, 'Loading module ' + module_name + '...')
    app_module = Configuration.new('configuration/' + module_name + '.json')
    # Run Module
    log(LOG_INFO, 'Run module ' + module_name + command_info)
    app_module.module.run command
  end
end