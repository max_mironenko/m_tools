# frozen_string_literal: true

require_relative '../module_interface'

# System Module (show Application help if Application not found)
class Empty < ModuleInterface
  # Execute module. Start point
  # @param [string] command
  # @raise
  def run(command)
    # Only show Application / Module Help
    puts @name
    puts @description
  end

  # Check if module ready to work
  # @return boolean
  # @raise
  def check
    true
  end
end