# frozen_string_literal: true

require 'json'

require_relative '../module_interface'

# Class check all database table, find fields with user information and save it in the JSON format
class CCPASearch < ModuleInterface
  # Execute module. Start point
  # @param [string] command
  # @raise
  def run(command)
    # show help
    if command == 'help'
      puts @name
      puts @description
      return
    end

    # check parameters
    if @parameters.include?('CCPA_fields') == false
      raise 'Configuration parameter CCPA_fields is mandatory for this Module'
    end

    # create data search result list
    @data_result = {}
    # find customer information
    find_data

    # check if any elements found
    raise 'No data found in the database' if @data_result.empty? == true

    # save result
    save_data
  end

  # Check if module ready to work
  # @return boolean
  # @raise
  def check
    true
  end

  private

  # finding customer data
  def find_data
    # load all tables
    tables = all_tables
    log(LOG_INFO, tables.size.to_s + ' tables found in the Database')

    # check each table
    tables.each do |element|
      element.values.each { |value| check_table value }
    end
  end

  # Get List of all tables in the Database
  # @return [Hash] of all tables in the Database
  def all_tables
    tables = @database.client.db_select('SHOW TABLES;')
    raise 'No Tables Found in the Database' if tables.nil? == true || tables.empty? == true

    tables
  end

  # Check columns in the table
  # @param [string] table_name
  def check_table(table_name)
    log(LOG_INFO, ' >> Analyze table "' + table_name + '"...')
    describe = @database.client.db_select('DESCRIBE %s;', table_name)
    # check each column
    describe.each do |element|
      # if Field column
      if element.key?('Field') == true
        # check if field may contain customer data
        check_column(table_name, element['Field'])
      end
    end
  end

  # Check if column can contain customer data
  # @param [string] table_name
  # @param [string] column_name
  # @return boolean
  def check_column(table_name, column_name)
    # get list of all columns with data from configuration
    @parameters['CCPA_fields'].each do |value|
      next if column_name.include?(value) != true

      log(LOG_INFO, '  > Column ' + column_name + ' was found in the "' + table_name + '" table')
      table_data = if @data_result.include?(table_name)
                     @data_result[table_name]
                   else
                     []
                   end
      table_data.push(column_name)
      @data_result.store(table_name, table_data)
    end
  end

  # Save customer data to the JSON file
  def save_data
    # get data in the JSON Format
    json_data = JSON.pretty_generate @data_result
    # get file name
    file_name = Time.now.to_i.to_s + '.json'
    # get file path
    file_path = Dir.pwd + '/storage/ccpa/' + file_name

    # save log to file
    file = File.open(file_path, 'w+')
    file.puts json_data
    file.close

    # show log
    log(LOG_INFO, 'File ' + file_name + ' was successfully saved in the Application Storage')
  end
end