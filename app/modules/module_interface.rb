# frozen_string_literal: true

# Interface for all Application Modules
class ModuleInterface < Helper
  attr_accessor :name, :description
  attr_accessor :database
  attr_accessor :parameters

  # Execute module. Start point
  # @param [string] command
  # @raise
  def run(command)
    raise 'The method is not implemented'
  end

  # Check if module ready to work
  # @return boolean
  # @raise
  def check
    raise 'The method is not implemented'
  end

  # Check if module contain Database class
  # @return boolean
  # @raise
  def database?
    false if @database.nil?
  end
end