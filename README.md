# README #

M-Tool - Modular Ruby Application working with Magento 1 and Magento 2 Database.

### About ###

* Module structure allows easy to add new features in Application. 
* Each Module is configuring by a simple JSON file.
* Each Module is independent.

* Version 1.1

### Application ###

To Run Module:
/usr/bin/ruby m_tools.rb module_name[:command]
[:command] - if necessary
Module name = configuration file from Configuration folder without ".json"

Example: /usr/bin/ruby m_tools.rb ccpa

To Run Module help:
/usr/bin/ruby m_tools.rb module_name:help

Example: /usr/bin/ruby m_tools.rb ccpa:help

Module help - field description from the module configuration file.

### Application Structure ###

* app/core - Application core files
* app/modules - Independent modules
* configuration - modules configuration files
* log - application logs
* storage - a place for module's output files
* m_tools.rb - Application start point

### Module CCPA ###

Task: Find all tables and columns where any personal customer information can be store. Save results in the JSON format.
The next module will copy or delete or obfuscate customer information using the collected data.

* Load all tables in the Database
* Load all columns for each table
* Compare columns with data provided in the configuration file
* Save the result in JSON format.

Example: 
Log file: /log/2020-7-12.log
JSON file: /storage/ccpa/1594616071.json